#!/bin/bash

TEST_PATCH="/tmp/known_bad.patch"

curl -s -o $TEST_PATCH https://patchwork.ozlabs.org/patch/1023062/mbox/

./skt.sh merge --workdir=$WORKDIR --fetch-depth=1 --ref=linux-4.20.y \
    --patch=${TEST_PATCH} \
    --baserepo=https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git

# The skt command should have failed with a 1.
if [ $? -ne 1 ]; then
    TESTEXIT=1
else
    TESTEXIT=0
fi

# Cleanup
rm -f $TEST_PATCH
unset TEST_PATCH

exit $TESTEXIT
