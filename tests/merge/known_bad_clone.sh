#!/bin/bash

./skt.sh merge --workdir=$WORKDIR --fetch-depth=1 --ref=v4.19.15 \
    --baserepo=https://git.kernel.org/pub/scm/linux/kernel/git/stable/meow.git

if [ $? -ne 2 ]; then
    EXITCODE=1
else
    EXITCODE=0
fi

rm -rf $WORKDIR
exit $EXITCODE
