cmd_build_help() {
    cat <<EOF
skt.sh build - compile a kernel

compile options:

  -c, --baseconfig      Path to a kernel config file to use
  --cfgtype             Kernel config type to use (default: olddefconfig)
                        Examples: tinyconfig, allyesconfig, rh-configs
  --enable-debuginfo    Enable debuginfo during compile (makes larger kernel)
  --localversion        Set the LOCALVERSION for the compiled kernel
                        (default: skt)
  --makeopts            Extra make options to pass to the compile step
  --rh-configs-glob     A glob string to find the correct Red Hat kernel config
                        (Required if using --cfgtype rh-configs)
  --wipe                Clean the kernel with `make mrproper` before compiling

other options:

  -s, --state           Path to state file
                        (default: 'sktrc' in current directory)
  -w, --workdir         Path to the work directory
                        (default: 'workdir' in current directory)

EOF
}

cmd_build() {
    # Set up options/arguments for getopt.
    SHORT_ARGS="c:,s:,w:"
    LONG_ARGS="baseconfig:,cfgtype:,enable-debuginfo,help,localversion:,makeopts:,make-target:,rh-configs-glob,state:,workdir:,wipe"
    GETOPT_CMD=`getopt -o ${SHORT_ARGS} --long ${LONG_ARGS} -- "$@"`
    eval set -- "$GETOPT_CMD"

    # Defaults for command line arguments.
    CONFIG_TYPE='olddefconfig'
    ENABLE_DEBUGINFO=0
    EXTRA_MAKEOPTS=''
    LOCALVERSION='skt'
    MAKE_TARGET='targz-pkg'
    RH_CONFIGS_GLOB=''
    WIPE=0

    while true ; do
        case "$1" in
            -c|--baseconfig) CONFIG_FILE=$2 ; shift 2 ;;
            --cfgtype) CONFIG_TYPE=$2 ; shift 2 ;;
            --enable-debuginfo) ENABLE_DEBUGINFO=1 ; shift 1 ;;
            --help) cmd_build_help ; exit 0 ;;
            --localversion) LOCALVERSION=$2 ; shift 2 ;;
            --makeopts) EXTRA_MAKEOPTS=$2 ; shift 2 ;;
            --make-target) MAKE_TARGET=$2 ; shift 2 ;;
            --rh-configs-glob) RH_CONFIGS_GLOB=$2 ; shift 2 ;;
            -s|--state) STATE=$2 ; shift 2 ;;
            --wipe) WIPE=1 ; shift 1 ;;
            -w|--workdir) WORKDIR=$2 ; shift 2 ;;
            --) shift ; break ;;
            *) echo "Internal error!" ; exit 1 ;;
        esac
    done

    # Initial variables.
    MERGE_OK=1
    KERNEL_FILE=''
    LOGFILE=${WORKDIR}/build.log
    BUILD_TIMEOUT=7200 # Set a 2 hour timeout for building the kernel

    # If the config type is 'rh-configs', then RH_CONFIGS_GLOB *must* be set.
    if [ "${CONFIG_TYPE}${RH_CONFIGS_GLOB}" == 'rh-configs' ]; then
        echo "The --rh-configs-glob argument must be used when --cfgtype "
        echo "is 'rh-configs'."
        exit $EXIT_CODE_FAILURE
    fi

    # Does the state file exist? If it doesn't, the user might not have run
    # the merge command yet (which is required).
    if [ ! -f "${STATE}" ]; then
        log_fail "The state file is not present: ${STATE}"
        exit $EXIT_CODE_INFRASTRUCTURE
    fi

    # Check to see if the kernel tree is present.
    VALID_SOURCE=1
    if [ -d "${WORKDIR}/source" ]; then
        pushd "${WORKDIR}/source"
            git status > /dev/null 2>&1
            if [ $? -ne 0 ]; then
                log_fail "No git tree found at ${WORKDIR}/source"
                VALID_SOURCE=0
            fi
        popd
    else
        log_fail "Kernel source not present at ${WORKDIR}/source"
        VALID_SOURCE=0
    fi

    # Abort if the kernel is not cloned where we expect it to be.
    if [ $VALID_SOURCE -eq 0 ]; then
        echo "Could not find a kernel source tree in the following location:"
        echo "  - ${WORKDIR}/source"
        echo "Have you run the 'merge' command yet?"
        exit $EXIT_CODE_FAILURE
    else
        log_pass "Kernel source tree found"
    fi

    # Create a build log file.
    date > $LOGFILE

    # If we were asked to wipe the repository first, let's do that.
    if [ $WIPE -eq 1 ]; then
        pushd "${WORKDIR}/source"
            log_msg "Cleaning kernel source with mrproper"
            make mrproper 2>&1 | tee -a $LOGFILE
            if [ $? -ne 0 ]; then
                log_fail 'Wipe failed'
            fi
        popd
    fi

    # Prepare the config based on the config type provided.
    case "$CONFIG_TYPE" in
        'rh-configs') make_redhat_configs $RH_CONFIGS_GLOB ;; # For Red Hat kernels
        'olddefconfig') make_olddefconfig $CONFIG_FILE ;; # Take an existing config and run olddefconfig.
        *) make_makefile_configs $CONFIG_TYPE ;; # Use the kernel makefile to make a config
    esac

    # Handle debuginfo based on user input.
    if [ $ENABLE_DEBUGINFO -eq 0 ]; then
        pushd ${WORKDIR}/source
            log_msg "Disabling DEBUG_INFO"
            scripts/config --disable DEBUG_INFO 2>&1 | tee -a $LOGFILE
        popd
    else
        pushd ${WORKDIR}/source
            log_msg "Enabling DEBUG_INFO"
            scripts/config --enable DEBUG_INFO 2>&1 | tee -a $LOGFILE
        popd
    fi

    # Set the LOCALVERSION
    LOCALVERSION=".${LOCALVERSION}"
    pushd ${WORKDIR}/source
        log_msg "Setting LOCALVERSION"
        scripts/config --set-str LOCALVERSION $LOCALVERSION 2>&1 | tee -a $LOGFILE
    popd

    # If we are building a tarball, we need to strip modules to save size.
    EXTRA_MAKEOPTS="INSTALL_MOD_STRIP=1 ${EXTRA_MAKEOPTS}"

    # Assemble the make options and write them to the state file.
    MAKE_OPTS="-j$(nproc) $MAKE_TARGET $EXTRA_MAKEOPTS"
    update_ini $STATE 'make_opts' "${MAKE_OPTS}"

    # Build the kernel.
    log_msg "Compiling the kernel"
    pushd "${WORKDIR}/source"
        # Set a timeout for building the kernel in case something causes it
        # to get stuck during a build.
        timeout $BUILD_TIMEOUT make $MAKE_OPTS 2>&1 | tee -a $LOGFILE

        # Possible exit codes:
        #   0:   Success
        #   124: Killed via timeout
        #   *:   Likely a true build failure
        if [ $? -eq 0 ]; then
            log_pass "Successfully built the kernel"
        elif [ $? -eq 124]; then
            log_fail "Kernel build reached timeout"
            exit $EXIT_CODE_INFRASTRUCTURE
        else
            log_fail "Kernel build failed"
            exit $EXIT_CODE_FAILURE
        fi
    popd

    # What is the kernel release version?
    pushd "${WORKDIR}/source"
        KERNEL_RELEASE=$(make kernelrelease)
    popd
    update_ini $STATE 'krelease' "$KERNELRELEASE"

    # Get the current SHA from the state file.
    BUILDHEAD=$(read_ini "$STATE" "buildhead")

    # Rename the config based on the commit SHA of the repo as it stands
    # after any patches are merged.
    RENAMED_CONFIG=${WORKDIR}/${BUILDHEAD}.config
    cp ${WORKDIR}/source/.config $RENAMED_CONFIG
    update_ini $STATE 'buildconf' "$RENAMED_CONFIG"

    # Look for a tarball in the output.
    TARBALL=$(grep -oP "^Tarball successfully created in \.\/\K(.*)$" $LOGFILE)
    if [ $? -eq 0 ]; then
        log_pass "Found kernel tarball packages"

        # Rename the tarball based on the commit SHA of the repo as it stands
        # after any patches are merged.
        RENAMED_TARBALL=${WORKDIR}/${BUILDHEAD}.tar.gz
        mv -v ${WORKDIR}/source/$TARBALL $RENAMED_TARBALL

        # Write the filename to the state file.
        KERNEL_FILE=$(realpath $RENAMED_TARBALL)
        update_ini $STATE 'tarpkg' "$KERNEL_FILE"
    fi

    # Look for any RPMs in the output.
    RPMS=$(grep -oP "^Wrote: \K(.*rpm)$" $LOGFILE)
    if [ $? -eq 0 ]; then
        log_pass "Found kernel RPM packages"

        # Move the RPMs to a repo directory.
        RPM_REPO=${WORKDIR}/rpm_repo_${BUILDHEAD}/
        mkdir -p $RPM_REPO
        while read -r line; do
            mv -v $RPM $RPM_REPO
        done <<< "$RPMS"

        # Create a yum/dnf repository.
        createrepo $RPM_REPO 2>&1 | tee -a $LOGFILE

        # Write the path to the repo to the state file.
        KERNEL_FILE=$RPM_REPO
        update_ini $STATE 'rpm_repo' "$KERNEL_FILE"
    fi

    # If we didn't find any tarballs or RPM files, something went wrong.
    if [ "$KERNEL_FILE" == '' ]; then
        log_fail "No kernel binary packages found."
        exit $EXIT_CODE_FAILURE
    fi

    # Record the kernel arch to the state file.
    if [ -z "${ARCH}" ]; then
        ARCH=$(uname -m)
    fi
    update_ini $STATE 'kernel_arch' "${ARCH}"

    # Record the cross compiler prefix to the state file.
    update_ini $STATE 'cross_compiler_prefix' "${CROSS_COMPILE:-}"
}
