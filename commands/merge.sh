cmd_merge_help() {
    cat <<EOF
skt.sh merge - prepare a git tree and (optionally) merge patches

git repository options:

  -b, --baserepo:       URL to the kernel git repository to clone (required)
  -d, --fetch-depth:    Set the git fetch depth (default: 1)
  -r, --ref:            git reference to checkout from the kernel git
                        repository before applying patches (default: master)

patch options:

  -p, --pw:             URL to a patchwork patch to apply (can be repeated)
  -l, --patch:          Path to a local patch to apply (can be repeated)

other options:

  -s, --state:          Path to state file
                        (default: 'sktrc' in current directory)
  -w, --workdir:        Path to the work directory
                        (default: 'workdir' in current directory)

EOF
}

cmd_merge() {
    # Set up options/arguments for getopt.
    SHORT_ARGS="b:,d:,h,l:,p:,r:,s:,w:"
    LONG_ARGS="baserepo:,help,fetch-depth:,patch:,pw:,ref:,state:,workdir:"
    GETOPT_CMD=`getopt -o ${SHORT_ARGS} --long ${LONG_ARGS} -- "$@"`
    eval set -- "$GETOPT_CMD"

    # Initial variables
    MERGE_OK=1

    # Defaults
    FETCH_DEPTH=1
    PATCHWORK=()
    LOCALPATCH=()
    REF='master'

    while true ; do
        case "$1" in
            -b|--baserepo) BASEREPO=$2 ; shift 2 ;;
            -d|--fetch-depth) FETCH_DEPTH=$2 ; shift 2 ;;
            -h|--help) cmd_merge_help ; exit 0 ;;
            -p|--pw) PATCHWORK+=("$2") ; shift 2 ;;
            -r|--ref) REF=$2 ; shift 2 ;;
            -s|--state) STATE=$2 ; shift 2 ;;
            -l|--patch) LOCALPATCH+=("$2") ; shift 2 ;;
            -w|--workdir) WORKDIR=$2 ; shift 2 ;;
            --) shift ; break ;;
            *) echo "Internal error!" ; exit 1 ;;
        esac
    done

    # Create the work directory and a directory to hold the git source.
    log_msg "Creating workdir and source directory"
    mkdir -p ${WORKDIR}/source

    # Ensure the repository is cloned with the correct ref checked out.
    pushd ${WORKDIR}/source
    log_msg "Preparing and cloning the git repository"
    $GITCMD init
    if ! $GITCMD remote get-url origin 2> /dev/null; then
        $GITCMD remote add origin $BASEREPO
    fi
    $GITCMD fetch origin +${REF}:refs/remotes/origin/${REF} --depth ${FETCH_DEPTH} && \
        $GITCMD checkout -q --detach refs/remotes/origin/${REF} && \
        $GITCMD reset --hard refs/remotes/origin/${REF}

    # Did our chain of git commands fail?
    if [ $? -ne 0 ]; then
        EXIT_CODE=2 # Infrastructure failure
        log_fail "The repository could not be cloned successfully."
        exit $EXIT_CODE
    else
        log_pass "Git repository is ready"
    fi

    # Gather data about the commit we've checked out.
    BASEHEAD=$(${GITCMD} show --format=%H -s)
    COMMITDATE=$(${GITCMD} show --format=%ct -s ${BASEHEAD})
    BASESUBJECT=$(${GITCMD} show --format=%s -s ${BASEHEAD})

    # Build our state file
    cat << EOF > $STATE
[state]
basehead = ${BASEHEAD}
baserepo = ${BASEREPO}
basesubject = ${BASESUBJECT}
commitdate = ${COMMITDATE}
workdir = ${WORKDIR}
EOF

    # Make a temporary directory to hold patches
    TMPDIR=$(mktemp -d -t skt.shXXXXXX)

    # Set up a merge log.
    date > merge.log

    # If we have patchwork patches, iterate over each and apply them.
    if [ ${#PATCHWORK[@]} -ne 0 ]; then
        apply_patches 'patchwork' $PATCHWORK
    fi

    # If we have any local patches, iterate over each and apply them.
    if [ ${#LOCALPATCH[@]} -ne 0 ]; then
        apply_patches 'localpatch' $LOCALPATCH
    fi

    # Gather data about the commit after any patching.
    BUILDHEAD=$(${GITCMD} show --format=%H -s)
    BUILDSUBJECT=$(${GITCMD} show --format=%s -s ${BASEHEAD})

    # Add the build data to the state file.
    echo "buildhead = ${BUILDHEAD}" >> $STATE
    echo "buildsubject = ${BUILDSUBJECT}" >> $STATE
    popd

    # If the merge failed, provide the path to the merge log in the state
    # file.
    if [[ $MERGE_OK -ne 0 ]]; then
        echo "mergelog = ${WORKDIR}/merge.log" >> $STATE
    fi

}
